package me.DoppelRR.PokEgoServer;

public class User {
	
	private int id;
	private Position position;
	private Usertype type;
	
	public User(int id, Position position, Usertype type) {
		this.id = id;
		this.setPosition(position);
		this.type = type;
	}
	
	public int getID() {
		return id;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
	
	public Usertype getType() {
		return type;
	}

	public void setType(Usertype type) {
		this.type = type;
	}
}
