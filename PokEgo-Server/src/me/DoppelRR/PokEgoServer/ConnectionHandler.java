package me.DoppelRR.PokEgoServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class ConnectionHandler implements Runnable {
	
	private Socket client;
	private Server server;
	private Integer userID;
	
	public ConnectionHandler(Socket newClient, Server newServer, Integer newUserID) {
		client = newClient;
		server = newServer;
		userID = newUserID;
	}
	
	
	@Override
	public void run() {
		try {
			
			User user = server.users.get(userID);
			
			while(server.running) {
				
				String[] message = receive(client).split(":");
				System.out.println(message[0]);
				switch(message[0]) {
				case "send":
					System.out.println("Receiving data from: " + user.getID());
					
					Position position = new Position(Double.parseDouble(message[1]),
							Double.parseDouble(message[2]), Double.parseDouble(message[3]));

					user.setPosition(position);
					System.out.println("Received data from: " + user.getID());
					break;
				case "receive":
					System.out.println("Start Sending data to: " + user.getID());
					//Send all userdata
					String data = " |";
					
					for (User u : server.users.values()) {
						if (!user.equals(u)) {
							data += u.getID() + ":" + u.getType().toString() + ":";
							Position p = u.getPosition();
							data += p.getPosX() + ":" + p.getPosY() + ":" + p.getPosZ() + "|";
						}
					}
					
					send(client, data);
					System.out.println("Send data \"" + data + "\" to: " + user.getID());
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void send(Socket socket, String message) throws IOException {
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		printWriter.print(message);
		printWriter.flush();
	}
	
	private String receive(Socket socket) throws IOException {
	  	BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	 	char[] buffer = new char[200];
	 	int length = bufferedReader.read(buffer, 0, 200); // blockiert bis Nachricht empfangen
	 	String message= new String(buffer, 0, length);
	 	return message;
	}
}
