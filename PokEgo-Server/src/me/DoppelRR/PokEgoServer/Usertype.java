package me.DoppelRR.PokEgoServer;

public enum Usertype {
	
	SPHERE,
	CUBE;
	
	public static Usertype fromString(String s) {
		switch(s.toLowerCase()) {
		case "sphere":
			return Usertype.SPHERE;
		case "cube":
			return Usertype.CUBE;
		default:
			return Usertype.CUBE;
		}
	}
}
