package me.DoppelRR.PokEgoServer;

public class Position {
	private double posX;
	private double posY;
	private double posZ;
	
	public Position(double posX, double posY, double posZ) {
		this.setPosX(posX);
		this.setPosY(posY);
		this.setPosZ(posZ);
	}

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public double getPosZ() {
		return posZ;
	}

	public void setPosZ(double posZ) {
		this.posZ = posZ;
	}
}
