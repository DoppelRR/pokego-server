package me.DoppelRR.PokEgoServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Random;

public class Server {
    
	ServerSocket server;
	
	HashMap<Integer, User> users = new HashMap<Integer, User>();
	
	boolean running = true;
	
	public Server(int port) {
		try {
			server = new ServerSocket(port);
			
			System.out.println("Waiting for Connections...");
			
			while (running) {
				Socket clientSocket = server.accept();
				System.out.println("New Client connected.");
				
				String rawUserData = receive(clientSocket);
				
				Position position = null;
				String[] data = rawUserData.split(":");
				position = new Position(Double.parseDouble(data[1]),
						Double.parseDouble(data[2]), Double.parseDouble(data[3]));
				
				User u = new User(getNewID(), position, Usertype.fromString(data[0]));
				
				users.put(u.getID(), u);
				
				System.out.println("User Object (Type: " + u.getType().toString() 
						+ ") for new Client created.");
				
				ConnectionHandler connectionHandler = new ConnectionHandler(clientSocket, this, u.getID());
				
				new Thread(connectionHandler).start();
				
				System.out.println("Handler started.");
			}
				
			server.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String receive(Socket socket) throws IOException {
	  	BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	 	char[] buffer = new char[200];
	 	int length = bufferedReader.read(buffer, 0, 200); // blockiert bis Nachricht empfangen
	 	String message= new String(buffer, 0, length);
	 	return message;
	}
	
	private Integer getNewID() {
		
		Random ran = new Random();
		Integer i = ran.nextInt();
		
		while (users.containsKey(i))
			i = ran.nextInt();
		
		return i;
	}
}	